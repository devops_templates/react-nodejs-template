provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "ec2-linux-for-react" {
  ami = "ami-04989b617bd4f8f95"
  instance_type = "t3.micro"
  key_name = "my_ssh_key"
}

resource "aws_security_group" "security-gr-for-react-ec2" {
  name = "Security-gr-for-react-ec2"

  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/16"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}