#!/bin/bash
yum -y update
yum -y install httpd

myip=`curl http://169.254.169.254/latest/meta-data/public-hostname`

cat <<EOF > /var/www/html/index.html
<html>
<h2>Build by power of Terraform: <font color = "red">v0.12.26</font></h2><br>
Owner: ${f_name} ${l_name} <br>

% { for x in names ~}               #Invalid value for "vars" parameter: vars map does not contain key "x",
Hello to ${x} from ${f_name}<br>    #referenced at user_data.sh.tpl:13,12-13.
% { endfor ~}

</html>
EOF

sudo service httpd start
chkconfig httpd on